use lambda::lambda;
use serde::{Deserialize, Serialize};
use lambda_runtime::{Error, Context};

// Define a structure to represent the input event
#[derive(Debug, Deserialize)]
struct LambdaEvent {
    input_number: i32,
}

// Define a structure to represent the output response
#[derive(Debug, Serialize)]
struct LambdaResponse {
    doubled_number: i32,
}

// Lambda handler function
fn handler(event: LambdaEvent, _: Context) -> Result<LambdaResponse, Error> {
    // Double the input number
    let doubled_number = event.input_number * 2;
    // Create a response object
    let response = LambdaResponse {
        doubled_number,
    };
    // Return the response
    Ok(response)
}

fn main() -> Result<(), Error> {
    // Start the Lambda runtime with the handler function
    lambda(handler)
}
