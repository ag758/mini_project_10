# Project Title: Dockerized Hugging Face Rust Transformer with AWS Lambda Deployment

## Overview
This project aims to Dockerize a Rust implementation of a transformer model from Hugging Face, deploy it as a serverless application using AWS Lambda, and implement a query endpoint to interact with the deployed model.

## Requirements
1. **Dockerize Hugging Face Rust Transformer**: Package the Rust implementation of the Hugging Face transformer model into a Docker container.
2. **Deploy Container to AWS Lambda**: Deploy the Docker container to AWS Lambda for serverless execution.
3. **Implement Query Endpoint**: Create an endpoint to accept queries and pass them to the deployed transformer model.
4. **Grading Criteria**:
   - Transformer Packaging (30%)
   - Serverless Deployment (30%)
   - Endpoint Functionality (30%)
   - Documentation (10%)

## Deliverables
1. **Dockerfile and Rust Code**: Dockerfile and Rust code for the transformer model.
2. **Screenshot of AWS Lambda**: A screenshot showing the deployed AWS Lambda function.
3. **cURL Request Against Endpoint**: Sample cURL request demonstrating interaction with the deployed endpoint.

## Steps to Reproduce
1. **Transformer Packaging**:
   - Clone the Rust implementation of the transformer model from the Hugging Face repository.
   - Create a Dockerfile to package the Rust code and its dependencies into a Docker container.
   - Build the Docker image locally to ensure it runs correctly.

2. **Serverless Deployment**:
   - Set up an AWS account if you haven't already.
   - Use AWS CLI or AWS Management Console to deploy the Docker container to AWS Lambda.
   - Configure Lambda function settings, memory, timeouts, etc., appropriately.

3. **Implement Query Endpoint**:
   - Define an API Gateway endpoint to trigger the Lambda function.
   - Implement the necessary code in the Lambda function to process incoming requests and invoke the transformer model.
   - Test the endpoint locally using tools like `curl` or Postman.

4. **Documentation**:
   - Write a README.md file detailing the project, including installation instructions, usage guide, and any other relevant information.
   - Provide instructions on how to interact with the deployed endpoint.
   - Include any troubleshooting tips or potential issues encountered during the development process.

### AWS Lambda Function: 
![alt text](assets/Aws-lambda.png)

### Docker Desktop Run
![alt text](assets/Docker_Desktop_Run.png)

### Query Results
![alt text](assets/query_results.png)



## Conclusion
This project demonstrates how to Dockerize a Hugging Face Rust transformer model, deploy it as a serverless application on AWS Lambda, and create a query endpoint to interact with the deployed model. By following the provided steps, you can replicate this setup and adapt it to deploy other transformer models or applications.

